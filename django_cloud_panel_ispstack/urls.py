from django.urls import path

from . import views

urlpatterns = [
    path("cpes", views.list_cpes),
    path("cpes/create", views.modal_cpe_create),
    path("cpe/<str:cpe>", views.edit_cpe),
    path("cpe/<str:cpe>/delete", views.modal_cpe_delete),
]
