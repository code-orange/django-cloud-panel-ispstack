from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.template import loader
from django.utils.translation import gettext as _

from django_cloud_panel_main.django_cloud_panel_main.views import get_nav
from django_session_ldap_attributes.django_session_ldap_attributes.decorators import (
    admin_group_required,
)


@admin_group_required("BROADBAND")
def list_cpes(request):
    template = loader.get_template("django_cloud_panel_ispstack/list_cpes.html")

    template_opts = dict()

    template_opts["content_title_main"] = _("Internet Access")
    template_opts["content_title_sub"] = _("List CPEs")

    template_opts["menu_groups"] = get_nav(request)

    return HttpResponse(template.render(template_opts, request))


@admin_group_required("BROADBAND")
def edit_cpe(request, cpe):
    template = loader.get_template("django_cloud_panel_ispstack/edit_cpe.html")

    template_opts = dict()

    template_opts["content_title_main"] = _("Internet Access")
    template_opts["content_title_sub"] = _("Edit CPE") + " " + cpe

    template_opts["menu_groups"] = get_nav(request)

    return HttpResponse(template.render(template_opts, request))


@admin_group_required("BROADBAND")
def modal_cpe_create(request):
    template = loader.get_template("django_cloud_panel_ispstack/modal_cpe_create.html")

    template_opts = dict()

    template_opts["content_title_main"] = _("Internet Access")
    template_opts["content_title_sub"] = _("Create CPE")

    return HttpResponse(template.render(template_opts, request))


@admin_group_required("BROADBAND")
def modal_cpe_delete(request, cpe):
    template = loader.get_template("django_cloud_panel_ispstack/modal_cpe_delete.html")

    template_opts = dict()

    template_opts["content_title_main"] = _("Internet Access")
    template_opts["content_title_sub"] = _("Delete CPE") + " " + cpe

    template_opts["cpe"] = cpe

    return HttpResponse(template.render(template_opts, request))
